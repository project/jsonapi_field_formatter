<?php declare(strict_types = 1);

namespace Drupal\jsonapi_field_formatter\EventSubscriber;

use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\jsonapi\Events\CollectResourceObjectMetaEvent;
use Drupal\jsonapi\Events\JsonapiEvents;
use Drupal\rest_views\SerializedData;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * JSON:API meta events subscriber.
 */
final class JsonApiMetaResourceCollector implements EventSubscriberInterface {

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The render service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * JsonApiMetaResourceCollector constructor.
   */
  public function __construct(RequestStack $request_stack, RendererInterface $renderer) {
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
  }

  /**
   * Acts on JsonapiEvents::COLLECT_RESOURCE_OBJECT_META event.
   */
  public function onCollectResourceObjectMeta(CollectResourceObjectMetaEvent $event): void {
    $display = $this->getEntityDisplay();
    if (!$display) {
      return;
    }

    $formatted = [];
    $resource_field_items = $event->getResourceObject()->getFields();
    $context = new RenderContext();

    foreach ($display->getComponents() as $name => $component) {
      $field_items = $resource_field_items[$name] ?? NULL;
      if ($field_items instanceof FieldItemListInterface) {
        $build = $field_items->view($component);

        $data_items = NULL;
        foreach (Element::children($build) as $delta) {
          if (($build[$delta]['#type'] ?? NULL) === 'data') {
            $data = $build[$delta]['#data'] ?? NULL;
            $data_items[$delta] = $data instanceof SerializedData ? $this->collectSerializedData($data) : $data;
          }
        }

        if ($data_items && !$field_items->getFieldDefinition()->getFieldStorageDefinition()->isMultiple()) {
          $data_items = reset($data_items);
        }

        $this->renderer->executeInRenderContext($context, function () use (&$formatted, $data_items, $name, $build) {
          $formatted[$name] = $data_items ?? $this->renderer->render($build);
        });
      }
    }

    if (!$context->isEmpty()) {
      $bubbleable_metadata = $context->pop();
      $event->addCacheableDependency($bubbleable_metadata);
    }

    $event->setMeta('formatted_fields', $formatted);
  }

  /**
   * Collects serialized data.
   *
   * @param \Drupal\rest_views\SerializedData $data
   *   A serialized data object.
   *
   * @return scalar|array
   *   Either a scalar value or an array of scalars.
   */
  protected function collectSerializedData(SerializedData $data) {
    $result = NULL;
    $data_value = $data->getData();

    if (is_scalar($data_value)) {
      $result = $data_value;
    }
    elseif (is_array($data_value) || is_object($data_value)) {
      if (is_object($data_value) && !($data_value instanceof \Traversable)) {
        $data_value = (array) $data_value;
      }
      foreach ($data_value as $key => $value) {
        if ($value instanceof SerializedData) {
          $value = $this->collectSerializedData($value);
        }
        $result[$key] = $value;
      }
    }

    return $result;
  }

  /**
   * Returns the current entity display.
   *
   * @return \Drupal\Core\Entity\Display\EntityDisplayInterface|null
   *   A display object or NULL, if none was requested.
   */
  protected function getEntityDisplay(): ?EntityDisplayInterface {
    $request = $this->requestStack->getCurrentRequest();
    $view_mode = $request->query->get('ViewMode');
    $entity = RouteMatch::createFromRequest($request)->getParameter('entity');
    $display = $view_mode && $entity instanceof FieldableEntityInterface ?
      EntityViewDisplay::collectRenderDisplay($entity, $view_mode) : NULL;
    return $display ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      JsonapiEvents::COLLECT_RESOURCE_OBJECT_META => 'onCollectResourceObjectMeta',
    ];
  }

}
