# JSON:API Field Formatter

Allows to specify a view mode in JSON:API requests and adds any formatted field
configured via the view mode to a JSON:API response `meta` attribute.

The view mode is specified via a `ViewMode` GET parameter, for instance:

https://example.org/jsonapi/node/article/913eab20-4a67-405a-96f4-e4e749b815e9?ViewMode=api

**Note:** this depends on [#3100732-21: Allow specifying `meta` data on JSON:API
objects](https://www.drupal.org/comment/14474283#comment-14474283).


## Installation

This assumes `composer` will be used to install the module and apply the patch.
You can find more information about alternative ways at:
- https://www.drupal.org/docs/extending-drupal/installing-modules
- https://www.drupal.org/docs/develop/git/using-git-to-contribute-to-drupal/applying-a-patch-in-a-feature-branch

Add the module to your project via the following command:

```
composer require drupal/jsonapi_field_formatter:dev-1.0.x
```

Add the following line to your `composer.json` or `composer.patches.json` file,
depending on how your `composer.json` file is structured (see [the related
documentation](https://www.drupal.org/docs/develop/git/using-git-to-contribute-to-drupal/applying-a-patch-in-a-feature-branch#s-composer)
for more details):

```
"Issue #3100732: Allow specifying `meta` data on JSON:API objects": "https://www.drupal.org/files/issues/2022-04-05/3100732-20.patch"
```

The end result should look more or less like this:

```
{
  "patches": {
    "drupal/core": {
      ...
      "Issue #3100732: Allow specifying `meta` data on JSON:API objects": "https://www.drupal.org/files/issues/2022-04-05/3100732-20.patch"
    },
    ...
  }
}
```

Run `composer install` to apply the patch and [enable the module as usual](https://www.drupal.org/docs/extending-drupal/installing-modules#s-step-2-enable-the-module).


## Usage

1. At `/admin/structure/display-modes/view` create a new view mode (e.g. `API`)
   for all the entity types you need to export. For instance, you may want to
   export a `node` with a `media` reference, so you would create an `API` view
   mode for both.
2. Configure the `API` view mode for each entity type, specifying which field
   formatters should be used.
   - For instance, for an `article` content type you
      would do this at `/admin/structure/types/manage/article/display`.
   - Enable `Custom display settings` at the bottom of the page for the `API`
      view mode and go to the newly available `API` tab.
   - The field formatters offered by the [REST Views](https://www.drupal.org/project/rest_views)
     module may turn out to be useful. For instance the `Export rendered entity`
     one might be a good choice for a media reference field.
     - In this case do not forget to select which view mode the entity should
       be rendered with, in this example at
       `/admin/structure/media/manage/image/display/api` (for the media `API`
       view mode).
3. After creating an entity, e.g. an article, in the JSON response returned at
   https://example.org/jsonapi/node/article/913eab20-4a67-405a-96f4-e4e749b815e9?ViewMode=api
   you will find a new `formatted_fields` item in the `data.meta` section:
   ```
    "meta": {
      "formatted_fields": {
        "field_image": {
          "media_image": {
            "url": "https:\/\/example.org\/sites\/default\/files\/2022-07\/test.jpg",
            "alt": "Test"
          }
        },
        "uid": "\n\u003Cspan\u003E\u003Cspan lang=\u0022\u0022 about=\u0022\/user\/1\u0022 typeof=\u0022schema:Person\u0022 property=\u0022schema:name\u0022 datatype=\u0022\u0022\u003Eadmin\u003C\/span\u003E\u003C\/span\u003E\n",
        "title": "\n\u003Cspan\u003ETest\u003C\/span\u003E\n",
        "created": "\n\u003Cspan\u003EMon, 07\/04\/2022 - 15:42\u003C\/span\u003E\n"
      }
    }
   ```

Note that fields not having a configurable display, for instance the node title,
are always included and get default formatting.
